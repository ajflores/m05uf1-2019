package com.itb2019;

public class Constants {

    public static final String CATALA = "ca";
    public static final String ESPANYOL = "es";
    public static final String INGLES = "en";

    public static final String ESPAI = " ";
}
